﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Checkout.Basket.WebApi.DataAccess.Entities;

namespace Checkout.Basket.Sdk
{
    public interface IOrdersApiClient
    {
        IList<Order> GetOrders();
        Order GetOrderById(int orderId);
        void CreateOrder(Guid userId);
        void AddOrderItem(int orderId, int itemId, decimal quantity);
        void DeleteOrderItem(int orderId, int itemId);
        void UpdateOrderItem(int orderId, int itemId, decimal quantity);
    }

    public class OrdersApiClient : IOrdersApiClient
    {
        private readonly IApiClient _apiClient;

        public OrdersApiClient(IApiClient apiClient, CheckoutConfiguration configuration)
        {
            _apiClient = apiClient;
        }

        public IList<Order> GetOrders()
        {
            throw new NotImplementedException();
        }

        public Order GetOrderById(int orderId)
        {
            throw new NotImplementedException();
        }

        public void CreateOrder(Guid userId)
        {
            throw new NotImplementedException();
        }

        public void AddOrderItem(int orderId, int itemId, decimal quantity)
        {
            throw new NotImplementedException();
        }

        public void DeleteOrderItem(int orderId, int itemId)
        {
            throw new NotImplementedException();
        }

        public void UpdateOrderItem(int orderId, int itemId, decimal quantity)
        {
            throw new NotImplementedException();
        }

        public Order GetOrder(int orderId)
        {
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Defines the configuration used to connect and authenticate to Checkout APIs.
    /// </summary>
    public class CheckoutConfiguration
    {
        public const string ProductionUri = "https://api.checkout.com/";
        public const string SandboxUri = "https://api.sandbox.checkout.com/";

        /// <summary>
        /// Creates a new <see cref="CheckoutConfiguration"/> instance.
        /// </summary>
        /// <param name="secretKey">Your secret key obtained from the Checkout Hub.</param>
        /// <param name="useSandbox">Whether to connect to the Checkout Sandbox. False indicates the live environment should be used.</param>
        public CheckoutConfiguration(string secretKey, bool useSandbox)
            : this(secretKey, useSandbox ? SandboxUri : ProductionUri)
        {

        }

        /// <summary>
        /// Creates a new <see cref="CheckoutConfiguration"/> instance, explicitly setting the API's base URI. 
        /// </summary>
        /// <param name="secretKey">Your secret key obtained from the Checkout Hub.</param>
        /// <param name="uri">The base URL of the Checkout API you wish to connect to.</param>
        public CheckoutConfiguration(string secretKey, string uri)
        {
            if (string.IsNullOrEmpty(secretKey)) throw new ArgumentException($"Your API secret key is required", nameof(secretKey));
            if (string.IsNullOrEmpty(uri)) throw new ArgumentException($"The API URI is required", nameof(uri));

            SecretKey = secretKey;
            Uri = uri;
        }

        /// <summary>
        /// Gets the secret key that will be used to authenticate to the Checkout API.
        /// </summary>
        public string SecretKey { get; }

        /// <summary>
        /// Gets the Uri of the Checkout API to connect to.
        /// </summary>
        public string Uri { get; }

        /// <summary>
        /// Gets or sets your public key as obtained from the Checkout Hub.
        /// </summary>
        public string PublicKey { get; set; }
    }

    /// <summary>
    /// Defines the operations used for communicating with Checkout.com APIs.
    /// </summary>
    public interface IApiClient
    {
        /// <summary>
        /// Executes a GET request to the specified <paramref="path"/>. 
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="credentials">The credentials used to authenticate the request.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <typeparam name="TResult">The expected response type to be deserialized.</typeparam>
        /// <returns>A task that upon completion contains the specified API response data.</returns>
        Task<TResult> GetAsync<TResult>(string path, IApiCredentials credentials, CancellationToken cancellationToken);

        /// <summary>
        /// Executes a POST request to the specified <paramref="path"/>. 
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="credentials">The credentials used to authenticate the request.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <param name="request">Optional data that should be sent in the request body.</param>
        /// <typeparam name="TResult">The expected response type to be deserialized.</typeparam>
        /// <returns>A task that upon completion contains the specified API response data.</returns>
        Task<TResult> PostAsync<TResult>(string path, IApiCredentials credentials, CancellationToken cancellationToken, object request = null);

        /// <summary>
        /// Executes a POST request to the specified <paramref="path"/>. 
        /// </summary>
        /// <param name="path">The API resource path.</param>
        /// <param name="credentials">The credentials used to authenticate the request.</param>
        /// <param name="resultTypeMappings">A dictionary of type mappings for different response codes.</param>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the underlying HTTP request.</param>
        /// <returns>A task that upon completion contains the specified API response data.</returns>
        /// <param name="request">Optional data that should be sent in the request body.</param>
        /// <returns>A task that upon completion contains the response type as determined by the <paramref="resultTypeMappings"/>.</returns>
        Task<dynamic> PostAsync(string path, IApiCredentials credentials, Dictionary<HttpStatusCode, Type> resultTypeMappings, CancellationToken cancellationToken, object request = null);
    }

    /// <summary>
    /// Defines an interface for different authorizatiopn/authentication schemes/types.
    /// </summary>
    public interface IApiCredentials
    {
        /// <summary>
        /// Authorizes the request.
        /// </summary>
        /// <param name="httpRequest">The HTTP request to authorize.</param>
        /// <returns>A task that completes when the provided <paramref="httpRequest"/> has been authorized.</returns>
        Task AuthorizeAsync(HttpRequestMessage httpRequest);
    }
}
