﻿namespace Checkout.Basket.WebApi.Models
{
    public class OrderItemModel
    {
        public int OrderId { get; set; }
        public int ItemId { get; set; }
        public decimal Quantity { get; set; }
    }
}