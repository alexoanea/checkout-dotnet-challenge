﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace Checkout.Basket.WebApi.DataAccess
{
    public class OrderContext : DbContext
    {
        public OrderContext() { }
        public OrderContext(DbContextOptions<OrderContext> options) : base(options) { }

        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrdersItems { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<OrderItem>().HasKey(o => new { o.OrderId, o.ItemId });
            modelBuilder.Entity<OrderItem>().Property(oi => oi.Quantity).IsRequired();
            modelBuilder.Entity<Order>().HasMany(o => o.OrderItems).WithOne(oi => oi.Order).HasForeignKey(oi => oi.OrderId);
            modelBuilder.Entity<Item>().HasMany(o => o.OrderItems).WithOne(oi => oi.Item).HasForeignKey(oi => oi.ItemId);
        }
    }
}
