﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Checkout.Basket.WebApi.DataAccess.Entities
{
    public class Order
    {
        //public Order()
        //{
        //    OrderItems = new List<OrderItem>();
        //}

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderId { get; set; }
        // Foreign Key
        public Guid UserId { get; set; }
        // Navigation property
        //public virtual User User { get; set; }
        public DateTime DateCreated { get; set; }
        // Collection navigation property
        public virtual IList<OrderItem> OrderItems { get; set; }
    }
}
