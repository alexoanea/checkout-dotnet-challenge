﻿using System.ComponentModel.DataAnnotations;

namespace Checkout.Basket.WebApi.DataAccess.Entities
{
    public class OrderItem
    {
        // Foreign Key
        public int OrderId { get; set; }
        // Navigation property
        public virtual Order Order { get; set; }
        // Foreign Key
        public int ItemId { get; set; }
        // Navigation property
        public virtual Item Item {get; set;}
        [Required]
        public decimal Quantity { get; set; }
    }
}
