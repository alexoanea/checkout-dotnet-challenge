﻿using Checkout.Basket.WebApi.DataAccess;
using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Checkout.Basket.WebApi.Helpers;
using Checkout.Basket.WebApi.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Checkout.Basket.WebApi.Infrastructure.Filters;
using Microsoft.AspNetCore.Http;

namespace Checkout.Basket.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment CurrentEnvironment { get; }
        public Startup(IConfiguration configuration, IHostingEnvironment currentEnvironment)
        {
            Configuration = configuration;
            CurrentEnvironment = currentEnvironment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc(options =>
                {
                    options.Filters.Add(typeof(HttpGlobalExceptionFilter));
                    options.Filters.Add(typeof(ValidateModelStateFilter));

                }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => 
                    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            ConfigureAuthService(services);

            if (CurrentEnvironment.IsEnvironment("Development"))
            {
                services.AddDbContext<OrderContext>(options =>
                    options.UseLazyLoadingProxies().UseInMemoryDatabase("CheckoutChallengeDB"));
            }
            else
            {
                services.AddDbContext<OrderContext>(options =>{
                    options.UseLazyLoadingProxies().UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), sqlServerOptionsAction: sqlOptions => {
                        sqlOptions.EnableRetryOnFailure(
                            maxRetryCount: 5,
                            maxRetryDelay: TimeSpan.FromSeconds(30),
                            errorNumbersToAdd: null
                        );
                    });
                });
                    
            }

            services.AddScoped<IAsyncRepository<Order>, EntityFrameworkRepository<Order>>();
            services.AddScoped<IAsyncRepository<Item>, EntityFrameworkRepository<Item>>();
            services.AddScoped<IAsyncRepository<OrderItem>, EntityFrameworkRepository<OrderItem>>();
            services.AddScoped<IAsyncRepository<User>, EntityFrameworkRepository<User>>();

            // configure DI for application services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IOrderService, OrderService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IIdentityService, IdentityService>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Checkout Basket API", Version = "v1", Description = "The Basket Service HTTP API", });

                // Swagger 2.+ support
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"                    
                });
                //c.AddSecurityRequirement(security);

                c.OperationFilter<AuthorizeCheckOperationFilter>();
            });
        }

        private void ConfigureAuthService(IServiceCollection services) {
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            // configure jwt authentication

            //// prevent from mapping "sub" claim to nameidentifier.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                var context = app.ApplicationServices.GetService<OrderContext>();
                AddTestData(context);
            }
            else
            {
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Checkout Basket API");
                c.RoutePrefix = string.Empty;
            });

            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();

            app.UseMvc();
        }

        private static void AddTestData(OrderContext context)
        {
            var userId = Guid.NewGuid();

            //seeding the in memory database for simplicity, 
            //in production applications password would be hashed 
            var testUser = new User
            {
                UserId = userId,
                FirstName = "Luke",
                LastName = "Skywalker",
                Username = "string",
                Password = "string",
            };
            var testItem1 = new Item
            {
                Name = "BB-8",
                Description = "Magnetic spherical balancing robot drive",
                SKU = "US8269447B2",
                Price = 99.99m
            };
            var testItem2 = new Item
            {
                Name = "R2D2",
                Description = "Astromech droid, skilled starship mechanic and fighter pilot's assistant",
                SKU = "US8269447B4",
                Price = 99.99m
            };
            var testOrder = new Order
            {
                UserId = userId,
                DateCreated = DateTime.UtcNow
            };
            var testOrderItem = new OrderItem
            {
                ItemId = 1,
                OrderId = 1,
                Quantity = 1
            };

            context.Users.Add(testUser);
            context.Items.Add(testItem1);
            context.Items.Add(testItem2);
            context.SaveChanges();
            context.Orders.Add(testOrder);
            context.SaveChanges();
            context.OrdersItems.Add(testOrderItem);
            context.SaveChanges();
        }
    }
}
