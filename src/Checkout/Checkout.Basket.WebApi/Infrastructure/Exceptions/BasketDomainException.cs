﻿using System;

namespace Checkout.Basket.WebApi.Infrastructure.Exceptions
{
    /// <summary>
    /// Exception type for Basket API exceptions
    /// </summary>
    public class BasketDomainException : Exception
    {
        public BasketDomainException()
        { }

        public BasketDomainException(string message)
            : base(message)
        { }

        public BasketDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
