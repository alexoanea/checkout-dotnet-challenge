﻿using Microsoft.AspNetCore.Http;
using System;

namespace Checkout.Basket.WebApi.Services
{
    public class IdentityService : IIdentityService
    {
        private IHttpContextAccessor _context;

        public IdentityService(IHttpContextAccessor context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public string GetUserIdentity()
        {
            return _context.HttpContext.User.FindFirst("unique_name").Value;
        }
    }
    public interface IIdentityService
    {
        string GetUserIdentity();
    }
}
