﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Checkout.Basket.WebApi.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Checkout.Basket.WebApi.Services
{
    public class UserService : IUserService
    {
        private readonly AppSettings _appSettings;
        private readonly IAsyncRepository<User> _asyncUserRepository;

        public UserService(IOptions<AppSettings> appSettings, IAsyncRepository<User> asyncUserRepository)
        {
            _appSettings = appSettings.Value;
            _asyncUserRepository = asyncUserRepository;
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = await _asyncUserRepository.FindAsync(x => x.Username == username && x.Password == password);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.UserId.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public async Task<IEnumerable<User>> GetAllAsync()
        {            
            var users =  await _asyncUserRepository.ListAllAsync();
            // return users without passwords
            return users.Select(x =>
            {
                x.Password = null;
                return x;
            });
        }

        public async Task<User> GetByIdAsync(Guid userId)
        {
            var user = await _asyncUserRepository.GetByIdAsync(userId);
            // return user without password
            user.Password = null;
            return user;
        }
    }

    public interface IUserService
    {
        Task<User> AuthenticateAsync(string username, string password);
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> GetByIdAsync(Guid userId);
    }
}
