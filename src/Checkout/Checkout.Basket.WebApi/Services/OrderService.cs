﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Checkout.Basket.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Checkout.Basket.WebApi.Services
{
    public interface IOrderService
    {
        Task<IEnumerable<Order>> GetAllAsync();
        Task<Order> GetOrderById(int orderid);
        Task CreateOrderForUser(Guid userId);
        Task AddItemAsync(OrderItemModel model);
        Task RemoveOrderItemAsync(int orderId, int itemId);
        Task UpdateOrderItemAsync(OrderItemModel model);
        Task RemoveOrderItems(int orderId);
    }
    public class OrderService : IOrderService
    {
        private readonly IAsyncRepository<Order> _asyncOrderRepository;
        private readonly IAsyncRepository<OrderItem> _asyncOrderItemRepository;
        public OrderService(IAsyncRepository<OrderItem> asyncOrderItemRepository, IAsyncRepository<Order> asyncOrderRepository)
        {
            _asyncOrderRepository = asyncOrderRepository;
            _asyncOrderItemRepository = asyncOrderItemRepository;
        }
        public async Task<IEnumerable<Order>> GetAllAsync()
        {
            var orders = await _asyncOrderRepository.ListAllAsync();
            return orders;
        }

        public async Task<Order> GetOrderById(int orderId)
        {
            var order = await _asyncOrderRepository.GetByIdAsync(orderId);
            return order;
        }

        public async Task CreateOrderForUser(Guid userId)
        {
            await _asyncOrderRepository.AddAsync(new Order
            {
                DateCreated = DateTime.UtcNow,
                UserId = userId
            });
        }

        public async Task AddItemAsync(OrderItemModel model)
        {
            await _asyncOrderItemRepository.AddAsync( new OrderItem {
                OrderId = model.OrderId,
                ItemId = model.ItemId,
                Quantity = model.Quantity
            });
        }

        public async Task RemoveOrderItemAsync(int orderId, int itemId)
        {
            var orderItem = await _asyncOrderItemRepository.FindAsync(oi => oi.OrderId.Equals(orderId) && oi.ItemId.Equals(itemId));
            await _asyncOrderItemRepository.DeleteAsync(orderItem);
        }

        public async Task UpdateOrderItemAsync(OrderItemModel model)
        {
            var orderItem = await _asyncOrderItemRepository.FindAsync(oi => oi.OrderId.Equals(model.OrderId) && oi.ItemId.Equals(model.ItemId));
            orderItem.Quantity = model.Quantity;
            await _asyncOrderItemRepository.UpdateAsync(orderItem);
        }

        public async Task RemoveOrderItems(int orderId)
        {
            var orderItems = await _asyncOrderItemRepository.Fetch(oi => oi.OrderId.Equals(orderId));
            foreach(var orderItem in orderItems)
            {
                await _asyncOrderItemRepository.DeleteAsync(orderItem);
            }
        }
    }
}
