﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.Models;
using Checkout.Basket.WebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Checkout.Basket.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]    
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        //JWT authentication endpoint
        //This would normally be part of a separate API in a microservice 
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateModel authModel)
        {
            var user = await _userService.AuthenticateAsync(authModel.Username, authModel.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        //This is only for development purpose
        //Obviously a normal user should not be able to get information about all the other users
        //In production this would be locked behind a special policy so that only specific users can access this resource
        [HttpGet]
        [ProducesResponseType(typeof(ICollection<User>), StatusCodes.Status200OK)]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult<IList<User>>> GetAll()
        {
            var users = await _userService.GetAllAsync();            
            return Ok(users);
        }

        //This is only for development purpose
        //Obviously a normal user should not be able to get information about any of the other users
        //In production this would be locked behind a special policy so that only specific users can access this resource
        [HttpGet("{userId}")]
        [ProducesResponseType(typeof(User), StatusCodes.Status200OK)]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult<User>> GetUserById(Guid userId)
        {
            var user = await _userService.GetByIdAsync(userId);
            return Ok(user);
        }
    }
}
