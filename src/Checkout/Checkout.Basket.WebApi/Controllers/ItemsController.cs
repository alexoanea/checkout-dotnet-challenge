﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Checkout.Basket.WebApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Checkout.Basket.WebApi.Controllers
{
    [Authorize]
    [Route("api/items")]
    [ApiController]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ItemsController : ControllerBase
    {
        private readonly IAsyncRepository<Item> _itemRepository;
        public ItemsController(IAsyncRepository<Item> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Item>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IList<Item>>> GetAll()
        {
            var items = await _itemRepository.ListAllAsync();
            return Ok(items);
        }

        [HttpGet("{itemId}")]
        [ProducesResponseType(typeof(Item), StatusCodes.Status200OK)]
        public async Task<ActionResult<Item>> Get(int itemId)
        {
            var item = await _itemRepository.GetByIdAsync(itemId);
            return Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ItemModel model)
        {
            try
            {
                var item = await _itemRepository.AddAsync(new Item
                {
                    Name = model.Name,
                    Description = model.Description,
                    Price = model.Price,
                    SKU = model.SKU
                });
                if (item != null)
                {
                    return CreatedAtAction("Get", "Items", new { itemId = item.ItemId }, item);
                }
                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest();
            }            
        }
    }
}
