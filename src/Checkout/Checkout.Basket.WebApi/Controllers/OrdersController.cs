﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.Models;
using Checkout.Basket.WebApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Checkout.Basket.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IIdentityService _identityService;

        public OrdersController(IOrderService orderService, IIdentityService identityService)
        {
            _orderService = orderService;
            _identityService = identityService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(ICollection<Order>), StatusCodes.Status200OK)]
        public async Task<ActionResult<IList<Order>>> GetAll()
        {
            var orders = await _orderService.GetAllAsync();
            return Ok(orders);

        }

        [HttpGet("{orderId}")]
        [ProducesResponseType(typeof(Order), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Order>> GetOrderById(int orderId)
        {
            var order = await _orderService.GetOrderById(orderId);
            if (order == null) 
            {
                return NotFound();
            }
            return Ok(order);
        }


        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> CreateOrder()
        {
            await _orderService.CreateOrderForUser(new Guid(_identityService.GetUserIdentity()));
            return Ok();
        }

        [HttpPost("{orderId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> AddOrderItem(int orderId, [FromBody] OrderItemModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _orderService.AddItemAsync(model);
            return Ok();
        }

        //This actually just removes all items in the order at the moment
        //Normally in a RESTful API this should also delete the order entity
        [HttpDelete("{orderId}")]
        public async Task<ActionResult> DeleteOrder(int orderId)
        {
            await _orderService.RemoveOrderItems(orderId);
            return Ok();
        }

        [HttpDelete("{orderId}/{itemId}")]
        public async Task<ActionResult> DeleteOrderItem(int orderId, int itemId)
        {
            await _orderService.RemoveOrderItemAsync(orderId, itemId);
            return Ok();
        }

        [HttpPut("{orderId}")]
        public async Task<ActionResult> UpdateOrderItem(int orderId, OrderItemModel model)
        {
            model.OrderId = orderId;
            await _orderService.UpdateOrderItemAsync(model);
            return Ok();
        }
    }
}