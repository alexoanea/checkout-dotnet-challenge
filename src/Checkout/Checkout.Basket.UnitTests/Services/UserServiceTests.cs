﻿using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Checkout.Basket.WebApi.Helpers;
using Checkout.Basket.WebApi.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Checkout.Basket.UnitTests.Services
{
    [TestClass]
    public class UserServiceTests
    {
        private readonly Mock<IAsyncRepository<User>> _userRepositoryMock;

        public UserServiceTests()
        {
            _userRepositoryMock = new Mock<IAsyncRepository<User>>();
        }

        [TestMethod]
        public async Task AuthenticateAsync_ReturnsExpectedEntity()
        {
            //Arange
            var fakeUserId = Guid.NewGuid();
            var fakeUser = GetUserFake(fakeUserId);
            IOptions<AppSettings> settings = Options.Create(new AppSettings());
            settings.Value.Secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";

            _userRepositoryMock.Setup(r => r.FindAsync(It.IsAny<Expression<Func<User, bool>>>())).ReturnsAsync(fakeUser);


            //Act
            var userService = new UserService(settings, _userRepositoryMock.Object);
            var actual = await userService.AuthenticateAsync(fakeUser.Username, fakeUser.Password);

            //Assert
            _userRepositoryMock.Verify(r => r.FindAsync(It.IsAny<Expression<Func<User, bool>>>()), times: Times.Once);
            Assert.AreEqual(actual, fakeUser);
        }

        [TestMethod]
        public async Task AuthenticateAsync_ReturnsNull()
        {
            //Arange
            var fakeUserId = Guid.NewGuid();
            var fakeUsername = "username";
            var fakePassword = "password";

            IOptions<AppSettings> settings = Options.Create(new AppSettings());
            settings.Value.Secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";

            //we force the repository to return null to fake wrong credentials behaviour
            _userRepositoryMock.Setup(r => r.FindAsync(It.IsAny<Expression<Func<User, bool>>>())).ReturnsAsync((User)null);


            //Act
            var userService = new UserService(settings, _userRepositoryMock.Object);
            var actual = await userService.AuthenticateAsync(fakeUsername, fakePassword);

            //Assert
            _userRepositoryMock.Verify(r => r.FindAsync(It.IsAny<Expression<Func<User, bool>>>()), times: Times.Once);
            Assert.AreEqual(actual, null);
        }

        private User GetUserFake(Guid userId)
        {
            return new User
            {
                UserId = userId,
                FirstName = "Luke",
                LastName = "Skywalker",
                Username = "string",
                Password = "string",
            };
        }
    }
}
