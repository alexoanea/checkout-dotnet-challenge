using Microsoft.VisualStudio.TestTools.UnitTesting;
using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.DataAccess.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Checkout.Basket.WebApi.Services;
using Checkout.Basket.WebApi.Models;
using System.Linq.Expressions;

namespace Checkout.Basket.UnitTests.Services
{
    [TestClass]
    public class OrderServiceTests
    {
        private readonly Mock<IAsyncRepository<Order>> _asyncOrderRepository;
        private readonly Mock<IAsyncRepository<OrderItem>> _asyncOrderItemRepository;

        public OrderServiceTests()
        {
            _asyncOrderRepository = new Mock<IAsyncRepository<Order>>();
            _asyncOrderItemRepository = new Mock<IAsyncRepository<OrderItem>>();
        }

        [TestMethod]
        public async Task GetOrderById_ReturnsExpectedEntity()
        {
            Order expected = new Order
            {
                OrderId = 1,
                DateCreated = DateTime.UtcNow,
                OrderItems = new List<OrderItem>(),
                UserId = Guid.NewGuid()
            };

            _asyncOrderRepository.Setup(r => r.GetByIdAsync(1)).ReturnsAsync(expected);


            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            var actual = await orderService.GetOrderById(1);

            _asyncOrderRepository.Verify(r => r.GetByIdAsync(1), times: Times.Once);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public async Task GetAllAsync_Calls_ListAllAsync_On_Repository()
        {
            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            var actual = await orderService.GetAllAsync();

            _asyncOrderRepository.Verify(r => r.ListAllAsync(), times: Times.Once);
        }

        [TestMethod]
        public async Task CreateOrderForUser_Calls_AddAsync_On_Repository()
        {
            Guid userId = Guid.NewGuid();

            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            await orderService.CreateOrderForUser(userId);

            _asyncOrderRepository.Verify(r => r.AddAsync(It.IsAny<Order>()), times: Times.Once);
        }

        [TestMethod]
        public async Task AddItemAsync_Calls_AddAsync_On_Repository()
        {
            OrderItemModel model = new OrderItemModel
            {
                ItemId = 1,
                OrderId = 1,
                Quantity = 1
            };

            OrderItem orderItem = new OrderItem
            {
                ItemId = model.ItemId,
                OrderId = model.OrderId,
                Quantity = model.Quantity
            };

            _asyncOrderItemRepository.Setup(r => r.AddAsync(orderItem)).ReturnsAsync(orderItem);

            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            await orderService.AddItemAsync(model);

            _asyncOrderItemRepository.Verify(r => r.AddAsync(It.Is<OrderItem>(oi => oi.ItemId.Equals(model.ItemId) && oi.OrderId.Equals(model.OrderId) && oi.Quantity.Equals(model.Quantity))), times: Times.Once);
        }


        [TestMethod]
        public async Task RemoveOrderItemAsync_Calls_FindAsync_and_DeleteAsync_On_OrderItemsRepository()
        {
            OrderItem orderItem = new OrderItem
            {
                ItemId = 1,
                OrderId = 1,
                Quantity = 1
            };

            _asyncOrderItemRepository.Setup(r => r.FindAsync(It.IsAny<Expression<Func<OrderItem, bool>>>())).ReturnsAsync(orderItem);

            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            await orderService.RemoveOrderItemAsync(orderItem.OrderId, orderItem.ItemId);

            _asyncOrderItemRepository.Verify(r => r.FindAsync(It.IsAny<Expression<Func<OrderItem, bool>>>()), times: Times.Once);
            _asyncOrderItemRepository.Verify(r => r.DeleteAsync(It.Is<OrderItem>(oi => oi.ItemId.Equals(orderItem.ItemId) && oi.OrderId.Equals(orderItem.OrderId) && oi.Quantity.Equals(orderItem.Quantity))), times: Times.Once);
        }

        [TestMethod]
        public async Task UpdateOrderItemAsync_Calls_FindAsync_and_UpdateAsync_On_OrderItemsRepository()
        {
            OrderItemModel model = new OrderItemModel
            {
                ItemId = 1,
                OrderId = 1,
                Quantity = 1
            };

            OrderItem orderItem = new OrderItem
            {
                ItemId = model.ItemId,
                OrderId = model.OrderId,
                Quantity = model.Quantity
            };

            _asyncOrderItemRepository.Setup(r => r.FindAsync(It.IsAny<Expression<Func<OrderItem, bool>>>())).ReturnsAsync(orderItem);

            var orderRepository = _asyncOrderRepository.Object;
            var orderItemRepository = _asyncOrderItemRepository.Object;
            var orderService = new OrderService(orderItemRepository, orderRepository);
            await orderService.UpdateOrderItemAsync(model);

            _asyncOrderItemRepository.Verify(r => r.FindAsync(It.IsAny<Expression<Func<OrderItem, bool>>>()), times: Times.Once);
            _asyncOrderItemRepository.Verify(r => r.UpdateAsync(It.Is<OrderItem>(oi => oi.ItemId.Equals(orderItem.ItemId) && oi.OrderId.Equals(orderItem.OrderId) && oi.Quantity.Equals(orderItem.Quantity))), times: Times.Once);
        }
    }
}