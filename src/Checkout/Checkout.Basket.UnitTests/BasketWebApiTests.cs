﻿using Checkout.Basket.WebApi.Controllers;
using Checkout.Basket.WebApi.DataAccess.Entities;
using Checkout.Basket.WebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Checkout.Basket.UnitTests
{
    [TestClass]
    public class BasketWebApiTests
    {
        private readonly Mock<IIdentityService> _identityServiceMock;
        private readonly Mock<IOrderService> _orderServiceMock;


        public BasketWebApiTests()
        {
            _identityServiceMock = new Mock<IIdentityService>();
            _orderServiceMock = new Mock<IOrderService>();
        }

        [TestMethod]
        public async Task GetOrderById_ReturnsExpectedEntity()
        {
            //Arrange
            var fakeUserId = Guid.NewGuid();
            var fakeOrder = GetFakeOrder(fakeUserId);

            _orderServiceMock.Setup(x => x.GetOrderById(1)).ReturnsAsync(fakeOrder);


            //Act
            var orderController = new OrdersController(_orderServiceMock.Object, _identityServiceMock.Object);

            var actionResult = await orderController.GetOrderById(fakeOrder.OrderId);

            //Assert
            _orderServiceMock.Verify(x => x.GetOrderById(It.Is<int>(i => i.Equals(fakeOrder.OrderId))), times: Times.Once);
            Assert.AreEqual((actionResult.Result as OkObjectResult).StatusCode, (int)System.Net.HttpStatusCode.OK);
            Assert.AreEqual(((Order)(actionResult.Result as OkObjectResult).Value).UserId, fakeUserId);
        }

        [TestMethod]
        public async Task GetOrderById_ReturnsNotFound()
        {
            //Arrange
            var fakeOrderId = 1;
            _orderServiceMock.Setup(x => x.GetOrderById(It.IsAny<int>())).ReturnsAsync((Order)null);

            //Act
            var orderController = new OrdersController(_orderServiceMock.Object, _identityServiceMock.Object);
            var actionResult = await orderController.GetOrderById(fakeOrderId);

            //Assert
            _orderServiceMock.Verify(x => x.GetOrderById(It.Is<int>(i => i.Equals(fakeOrderId))), times: Times.Once);
            Assert.AreEqual((actionResult.Result as NotFoundResult).StatusCode, (int)System.Net.HttpStatusCode.NotFound);
        }

        private Order GetFakeOrder(Guid fakeUserId)
        {
            return new Order
            {
                UserId = fakeUserId,
                DateCreated = DateTime.UtcNow,
                OrderId = 1,
                OrderItems = new List<OrderItem>()
            };
        }
    }
}
